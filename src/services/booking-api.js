'use strict'

module.exports = ({ docs }) => ({
  prefix: '/booking-api',
  prefixRewrite: '/booking-api',
  target: 'http://booking-api:3000',
  docs: docs('Booking API', '/swagger'),
  middlewares: [

  ]
})
