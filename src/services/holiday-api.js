'use strict'

module.exports = ({ docs }) => ({
  prefix: '/holiday-api',
  prefixRewrite: '/holiday-api',
  target: 'http://holiday-api:3000',
  docs: docs('Holiday API', '/swagger'),
  middlewares: [

  ]
})
