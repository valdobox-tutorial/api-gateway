'use strict'

module.exports = ({ docs }) => ({
  prefix: '/customer-api',
  prefixRewrite: '/customer-api',
  target: 'http://customer-api:3000',
  docs: docs('Customer API', '/swagger'),
  middlewares: [

  ]
})
