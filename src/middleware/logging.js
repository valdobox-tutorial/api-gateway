'use strict'

const morgan = require('morgan')
const iu = require('middleware-if-unless')()

morgan.token('user-id', (req, res) => {
  return req.user ? req.user.sub : ''
})
morgan.token('request-id', (req, res) => {
  return req.headers['x-request-id']
})
morgan.token('cache-hit', (req, res) => {
  return res.getHeader('x-cache-hit') === '1'
})
morgan.token('req-headers', (req, res) => {
  const headers = { ...req.headers }
  if (res.statusCode !== 401 || req.user) {
    delete headers.authorization
  }

  return headers
})
morgan.token('res-headers', (req, res) => {
  const headers = { ...res.getHeaders() }

  return headers
})

function jsonFormat (tokens, req, res) {
  return JSON.stringify({
    userId: tokens['user-id'](req, res),
    date: tokens.date(req, res, 'iso'),
    requestId: tokens['request-id'](req, res),
    responseTime: Number(tokens['response-time'](req, res)),
    remoteAddress: tokens['remote-addr'](req, res),
    method: tokens.method(req, res),
    url: tokens.url(req, res),
    statusCode: parseInt(tokens.status(req, res)),
    contentLength: parseInt(tokens.res(req, res, 'content-length') || 0),
    userAgent: tokens['user-agent'](req, res),
    cacheHit: tokens['cache-hit'](req, res),
    reqHeaders: tokens['req-headers'](req, res),
    resHeaders: tokens['res-headers'](req, res)
  })
}

const middleware = morgan(jsonFormat)

module.exports = iu(middleware)
