'use strict'

const bodyParser = require('body-parser')
const typeis = require('type-is')

module.exports = [
  // parse application/x-www-form-urlencoded
  bodyParser.urlencoded({ extended: false, limit: '5mb' }),
  // parse application/json
  bodyParser.json({ type: 'application/json', strict: true, limit: '5mb' }),
  // parse text/plain
  bodyParser.text({ limit: '5mb' }),
  // remove default parser value if content is missing
  (req, res, next) => {
    if (!typeis.hasBody(req)) {
      req.headers['content-length'] = '0'
      req.body = undefined
    } else if (req.headers['content-type'] && req.headers['content-type'].startsWith('multipart/form-data')) {
      // proxy raw upload requests to downstream service
      req.body = undefined
    }
    next()
  }
]
