'use strict'

module.exports = (req, res, next) => {
  if (req.user) {
    // caches are created per user to guarantee proper authorization model
    req.cacheAppendKey = (req) => req.user.sub
  }

  return next()
}
