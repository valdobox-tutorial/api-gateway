'use strict'

const iu = require('middleware-if-unless')()
const jwtMiddleware = require('express-jwt')
const boom = require('@hapi/boom')
const jwt = require('jsonwebtoken')

const middleware = ({ secrets, maxAge }) => {
  const middleware = async (req, res, next) => {
    try {
      if (!req.headers.authorization || !req.headers.authorization.includes('Bearer ')) {
        res.writeHead(401, { 'WWW-Authenticate': 'Basic realm="API Gateway Basic Auth"' })
        res.end('Forbidden!')

        return
      }

      // decode JWT token and identify the keycloak cluster
      const { iss } = jwt.decode(req.headers.authorization.split(' ')[1])

      // get issues secret or public key
      const secret = secrets[iss]
      if (!secret) {
        throw boom.unauthorized('Unknown keycloak provider!')
      }

      // create next JWT auth middleware using the target public key
      const jwtAuth = jwtMiddleware({ secret, maxAge })

      // run JWT verification
      return jwtAuth(req, res, next)
    } catch (err) {
      return next(err)
    }
  }

  return iu(middleware)
}

module.exports = middleware
