'use strict'

const iu = require('middleware-if-unless')()
const { ENVIRONMENT } = process.env
const REDIS_ENVS = ['production']

const store = REDIS_ENVS.includes(ENVIRONMENT) ? require('./../lib/redis-cache-store') : require('./../lib/ram-cache-store')

const middleware = require('http-cache-middleware')({
  stores: [store]
})

module.exports = iu(middleware)
