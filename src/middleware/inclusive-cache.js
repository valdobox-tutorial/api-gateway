'use strict'

const iu = require('middleware-if-unless')()

const middleware = (req, res, next) => {
  // disable exclusive cache strategy
  delete req.cacheAppendKey

  return next()
}

module.exports = iu(middleware)
