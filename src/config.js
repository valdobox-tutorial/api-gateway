'use strict'

const inclusiveCache = require('./middleware/inclusive-cache')
const docs = (name, endpoint) => ({ name, endpoint, type: 'swagger' })
const unsecureEndpoints = require('./lib/unsecure-endpoints')

module.exports = ({ client }) => {
  const services = require('./lib/import-services')({
    client,
    docs,
    inclusiveCache
  })

  return {
    // global middlewares
    middlewares: [
      // CORS middleware
      require('cors')(),

      // query parser
      require('connect-query')(),

      // body parser
      ...require('./middleware/body-parser'),

      // HTTP requests logging
      require('./middleware/logging').unless(unsecureEndpoints),

      // JWT verification middleware
      require('./middleware/authentication')({
        maxAge: process.env.JWT_MAX_AGE || '1 minute',
        secrets: {
          'http://localhost:8080/auth/realms/valdobox-tutorial': process.env.KEYCLOAK_PUBLIC_KEY,
          'https://keycloak.valdobox/auth/realms/valdobox-tutorial': process.env.KEYCLOAK_PUBLIC_KEY
        }
      }).unless(unsecureEndpoints),

      // exclusive cache
      require('./middleware/exclusive-cache'),

      // cache plugin
      require('./middleware/cache')
    ],

    // services proxy configuration
    routes: [
      ...services
    ]
  }
}
