'use strict'

// creating a restana application
const restana = require('restana')({
  errorHandler: require('./lib/error-handler')()
})

const PORT = process.env.PORT
const gateway = require('fast-gateway')
const axios = require('axios')

const client = axios.create({ baseURL: process.env.GATEWAY_TARGET_OVERRIDE || `http://127.0.0.1:${PORT}` })
const config = require('./config')({ client })
config.targetOverride = process.env.GATEWAY_TARGET_OVERRIDE
config.server = restana

const service = gateway(config)
const context = {
  client,
  service,
  port: PORT
}

// JWT health check endpoint
service.get('/jwt-check', (req, res) => res.send(req.user))

// health check
service.get('/health', (req, res) => res.send({
  status: 'OK'
}))

// cache cleanup
service.delete('/cache/expire', (req, res) => {
  // @TODO: Add authorization here
  res.setHeader('x-cache-expire', req.query.pattern)
  res.end()
})

module.exports = context
