'use strict'

const fs = require('fs')
const path = require('path')

module.exports = (ctx) => {
  const dir = path.join(__dirname, '/../services')
  const files = fs.readdirSync(dir)

  const services = files.map(file => require(path.join(dir, file))(ctx))

  return services
}
