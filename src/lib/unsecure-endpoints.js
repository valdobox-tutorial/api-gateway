'use strict'

module.exports = req => req.url === '/health'
