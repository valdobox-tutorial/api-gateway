'use strict'

const CacheManager = require('cache-manager')

module.exports = CacheManager.caching({
  store: 'memory', // RAM store
  max: 5000, // maximum elements on the store at once
  ttl: 30 // default ttl if omitted
})
