'use strict'

const CacheManager = require('cache-manager')
const logger = require('./logger')

const redisCache = module.exports = CacheManager.caching({
  store: require('cache-manager-ioredis'),
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  tls: {
    checkServerIdentity: (servername, cert) => {
      // skip certificate hostname validation
      return undefined
    }
  },
  password: process.env.REDIS_PASS,
  ttl: 30
})

const client = redisCache.store.getClient()
client.on('error', (err) => {
  logger.error(err, 'Cache redis client error')
})
client.on('connect', () => {
  logger.info('Cache redis client connected!')
})
client.on('reconnecting', () => {
  logger.info('Cache redis client reconnecting...')
})
client.on('end', (err) => {
  logger.error(err, 'Cache redis client connection ended!')
})
