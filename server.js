'use strict'

require('dotenv').config()
const logger = require('./src/lib/logger')

try {
  const { service, port } = require('./src/gateway')

  // start the gateway HTTP server
  service.start(port).then(() => logger.info(`API Gateway listening on ${port} port!`))
} catch (err) {
  logger.error('Unable to bootstrap the gateway service:', err)
  process.exit(1)
}
