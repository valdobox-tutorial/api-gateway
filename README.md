# Introduction
Node.js based API gateway for Holidays (uses the [fast-gateway](https://github.com/jkyberneees/fast-gateway#readme) library). Here we centralize and perform the following global workflows:
- Holidays HTTP requests proxy: Through routes configuration, we are able to route HTTP requests to target services by using a prefix discriminator. See `src/services` folder for more details. 
- Basic, JWT
- Generic Authorization. Simple authorization policies based on roles and groups and also evaluate for some services.
- HTTP Requests Logging.
- HTTP Caching. (Technical spec: https://github.com/jkyberneees/fast-gateway#gateway-level-caching)

# Configuration example
```js
PORT: 3000
SERVICE: 'api-gateway'
LOG_FORMAT: 'json' 
REDIS_HOST: 'api-gateway-redis-master'
ENVIRONMENT: 'develop'
CLOAKER_URL: 'http://localhost:8082/cloaker'
HOLIDAY_API_KEY: '*****'
REDIS_PORT: '6379'
KEYCLOAK_PUBLIC_KEY: '-----BEGIN PUBLIC KEY-----\n****\n-----END PUBLIC KEY-----'
```

# Local Development
```bash
# install dependencies
npm i

# run tests
npm test

# run locally
npm start
```

# Dependencies to other services?
This project have the following dependencies to other services:
- **redis**: HTTP caching middleware is setup to store the caches in Redis

# Service Endpoints
- `GET /health`: Service healthcheck
- `GET /jwt-check`: Get user details from authentication token.
