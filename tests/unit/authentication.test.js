'use strict'

/* eslint-env jest */
const SAMPLE_JWT = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJqMTRjZFllUEJ3Y1RkY01ITkNMTmVGOEdnLTk5TkZTZ3YtZUMtZExWaW9BIn0.eyJleHAiOjE1ODk0MDM1OTcsImlhdCI6MTU4OTQwMzI5NywiYXV0aF90aW1lIjoxNTg5NDAzMTg0LCJqdGkiOiIyNTFlOTEzOC0xMzY1LTQwMmQtODJkZi04ZDQxZGJhY2JjOWIiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvdmFsZG9ib3gtdHV0b3JpYWwiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiNmExYWI0ZDktNzI5YS00NDllLWE2MWItZGZjN2MyMmRjOTZkIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYXBpLWJyb3dzZXIiLCJub25jZSI6ImQyNTg0MTEzLTY4MTEtNDFmZS04NzBiLTg5MjY5YjRkMWQ1YyIsInNlc3Npb25fc3RhdGUiOiI1ZTg0YTgyZS1jYTVkLTQ3NWEtYTUxNC1iOTgzMzAwMmQ4NmMiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly9sb2NhbGhvc3Q6ODA4MSJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJhZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIiwib3BlcmF0b3IiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJUcmlwIEFkbWluIiwicHJlZmVycmVkX3VzZXJuYW1lIjoidHJpcGFkbWluIiwiZ2l2ZW5fbmFtZSI6IlRyaXAiLCJmYW1pbHlfbmFtZSI6IkFkbWluIn0.APgUCGsQTVUl5f5lK9Kg2DlGpzPUfjEInRycoQsBGTsjooe9fQ6fP3kxcdODYipHduKX4ar7YybEL7Kb0ME2wqCCfZRdxOcwVyHpqL5V5dADSDSqHIouCz8EHYnFYWinYWow__t0zQ9sp0wKKrCCHcwT9aJGOdfqXhPRLllxFPqNDtoVi5hGTWkq37Ne6X45tO7O31SahIhfG3aPJnPV5EQJNY_n_OtZXQY5IrOKxQj4oYZuD7UQFWEl4saCfsw1P2Ei_mJPSFQXIOHG2zFOjQyHyiqX3zynKN_36YfsbmEMBKj9ONqu7QxQIpVTcXzAuZUvM6n0-DFTtWE5Wh9hkg'

const mockAxios = jest.fn(() => Promise.resolve({
  data: {
    token: SAMPLE_JWT
  }
}))
const mockJwtAuth = jest.fn((req, res, next) => next())

jest.mock('axios', () => mockAxios)
jest.mock('express-jwt', () => () => mockJwtAuth)

const authMiddleware = require('../../src/middleware/authentication')
const auth = authMiddleware({
  secrets: {
    'http://localhost:8080/auth/realms/valdobox-tutorial': '***'
  }
})

afterEach(() => jest.clearAllMocks())

test('should perform jwt auth', done => {
  mockJwtAuth.mockImplementationOnce((req, res, next) => next())

  auth({ headers: { authorization: `Bearer ${SAMPLE_JWT}` } }, {}, () => {
    expect(mockJwtAuth).toBeCalledWith(
      { headers: { authorization: `Bearer ${SAMPLE_JWT}` } },
      {},
      expect.any(Function)
    )
    done()
  })
})
