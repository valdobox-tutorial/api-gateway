'use strict'

/* eslint-env jest */
require('dotenv').config()
const SIGNING_KEY = process.env.KEYCLOAK_CA_PUBLIC_KEY = '123456789'

const { service, client, port } = require('./../../src/gateway')
const jwt = require('jsonwebtoken')
const token = jwt.sign({ iss: 'http://localhost:8080/auth/realms/valdobox-tutorial', foo: 'bar', resource_access: { 'api-browser': { roles: ['sys-admin'] } } }, SIGNING_KEY)

const nock = require('nock')

nock('http://customer-api:3000')
  .get('/customer-api/customers/customerId1')
  .reply(200)
  .persist()

// faking JWT request token
client.defaults.headers.common.Authorization = `Bearer ${token}`

beforeAll(async () => {
  await service.start(port)
})

afterAll(() => {
  service.getServer().close()
})

test('proxy fails if JWT is missing', async () => {
  try {
    await client.get('/customer-api/customers/customerId1', {
      headers: {
        authorization: ''
      }
    })
  } catch (err) {
    expect(err.response.status).toEqual(401)
  }
})

test('gateway health check (no JWT) - /health', async () => {
  await client.get('/health', {
    headers: {
      authorization: ''
    }
  })
})
